<!doctype html>
<html lang="en">
<?php include ('ih_head.php');?>
<body>

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<div class="vk-sparta-transparents-1">
    <!-- Mobile nav -->
    <?php include ('ih_mobileNav.php'); ?>
    <!-- End mobile menu -->

    <div id="wrapper-container" class="site-wrapper-container">
        <!-- Main navigation -->
        <?php include ('ih_mainNav.php'); ?>
        <!-- end of Main navigation -->


        <!--BENGIN Reviews and Comments-->
        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Reviews and Comments</h2>

                        </div>
                    </div>
                </div>
            </div>
            <!--Comments and reviews body-->
            <div class="vk-posts-details-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="vk-posts-details-body-left">
                                <div class="vk-event-details-left-comment">
                <h4>4 comments</h4>
                <ul>
                    <li>
                        <div class="vk-event-details-comments-img">
                            <img src="../images/06_03_event_detail/comment/img.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="vk-event-details-comments-text">
                            <div class="vk-events-details-author">
                                <span class="authour-name">Alex</span>
                                <span class="author-time">09 may 2017 </span>
                            </div>
                            <div class="vk-event-details-des">
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada
                                    fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
                                    ultricies eget, tempor sit amet, ante. Donec eu libero sit amet
                                    quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                                    eleifend leo.
                                </p>
                            </div>
                            <div class="vk-event-details-btn">
                                <a href="#">Reply <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                <a href="#">20<i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                                <a href="#">10<i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vk-event-details-comments-img">
                            <img src="../images/06_03_event_detail/comment/img1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="vk-event-details-comments-text">
                            <div class="vk-events-details-author">
                                <span class="authour-name">Jessica</span>
                                <span class="author-time">09 may 2017 </span>
                            </div>
                            <div class="vk-event-details-des">
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada
                                    fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
                                    ultricies eget, tempor sit amet, ante. Donec eu libero sit amet
                                    quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                                    eleifend leo.
                                </p>
                            </div>
                            <div class="vk-event-details-btn">
                                <a href="#">Reply <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                <a href="#">20<i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                                <a href="#">10<i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vk-event-details-comments-img">
                            <img src="../images/06_03_event_detail/comment/img1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="vk-event-details-comments-text">
                            <div class="vk-events-details-author">
                                <span class="authour-name">Jessica</span>
                                <span class="author-time">09 may 2017 </span>
                            </div>
                            <div class="vk-event-details-des">
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada
                                    fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
                                    ultricies eget, tempor sit amet, ante. Donec eu libero sit amet
                                    quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                                    eleifend leo.
                                </p>
                            </div>
                            <div class="vk-event-details-btn">
                                <a href="#">Reply <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                <a href="#">20<i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                                <a href="#">10<i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="vk-event-details-comments-img">
                            <img src="../images/06_03_event_detail/comment/img1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="vk-event-details-comments-text">
                            <div class="vk-events-details-author">
                                <span class="authour-name">Jessica</span>
                                <span class="author-time">09 may 2017 </span>
                            </div>
                            <div class="vk-event-details-des">
                                <p>
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada
                                    fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae,
                                    ultricies eget, tempor sit amet, ante. Donec eu libero sit amet
                                    quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat
                                    eleifend leo.
                                </p>
                            </div>
                            <div class="vk-event-details-btn">
                                <a href="#">Reply <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                <a href="#">20<i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                                <a href="#">10<i class="fa fa-thumbs-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
                                <!--Add a comment-->

                                 <div class="vk-event-details-left-add-comment">
                <h4> Add Your Comments</h4>
                <div class="row">
                    <form action="#">
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="text" id="yourName" placeholder="Your name ..." class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="email" id="youremail" placeholder="Your email ..." class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <textarea class="form-control" id="message" name="message" placeholder="Message" rows="5"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="vk-event-details-submit">
                                    <a href="#">SUBMIT</a>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
                                <!--Add a comment-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--END of Reviews and Comments-->


        <footer class="site-footer footer-default">
            <div class="footer-main-content">
                <div class="container">
                    <div class="row">
                        <div class="footer-main-content-elements">
                            <div class="footer-main-content-element col-sm-4">
                                <aside class="widget">
                                    <div class="vk-widget-footer-1">
                                        <div class="widget-title">
                                            <a href="#"><img src="../images/logo-footer.png" alt="" class="img-responsive"></a>
                                        </div>
                                        <div class="widget-content">
                                            <div class="vk-widget-content-info">
                                                <p><span class="ti-mobile"></span> (+233) 123 456789</p>
                                                <p><span class="ti-email"></span> contact@sparta.com</p>
                                                <p><span class="ti-location-pin"></span> 45 Queen's Park Rd, Brighton, BN2 0GJ, UK</p>
                                            </div>
                                            <div class="vk-widget-content-share">
                                                <p>
                                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-2">
                                <aside class="widget">
                                    <div class="vk-widget-footer-2">
                                        <h3 class="widget-title"> Company</h3>
                                        <div class="widget-content">
                                            <ul class="vk-widget-content-2">
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Home</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> About us</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Testimonial</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Blog</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Contact</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-2">
                                <aside class="widget">
                                    <div class="vk-widget-footer-3">
                                        <h3 class="widget-title">Services</h3>
                                        <div class="widget-content">
                                            <ul class="vk-widget-content-2">
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Spa</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Rooms</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Restaurant</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Gym</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Promotion</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                            <div class="footer-main-content-element col-sm-4">
                                <aside class="widget">
                                    <div class="vk-widget-footer-4">
                                        <h3 class="widget-title">Sign up for newletter</h3>
                                        <div class="widget-content">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="email" class="form-control" placeholder="Your Email...">
                                                    <span class="input-group-addon success"><i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="vk-widget-trip">
                                                <a href="#"><img src="../images/01_01_default/stripad.png" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="copyright-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="copyright-text">
                                    <span>Sparta Plaza</span> -  Hotel & Resort PSD Template. Design by <span><a href="#">UniverTheme</a></span>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <ul class="copyright-menu">
                                    <li><a href="#">Term & Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Site Map</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery1.min.js"></script>
    <script src="../plugin/dist/owl.carousel.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.waypoints.js"></script>
    <script src="../js/number-count/jquery.counterup.min.js"></script>
    <script src="../js/isotope.pkgd.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/bootstrap-datepicker.tr.min.js"></script>
    <script src="../js/moment.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script src="../js/picturefill.min.js"></script>
    <script src="../js/lightgallery.js"></script>
    <script src="../js/lg-pager.js"></script>
    <script src="../js/lg-autoplay.js"></script>
    <script src="../js/lg-fullscreen.js"></script>
    <script src="../js/lg-zoom.js"></script>
    <script src="../js/lg-hash.js"></script>
    <script src="../js/lg-share.js"></script>
    <script src="../js/jquery.nice-select.js"></script>
    <script src="../js/semantic.js"></script>
    <script src="../js/parallax.min.js"></script>
    <script src="../js/jquery.nicescroll.min.js"></script>
    <script src="../js/jquery.sticky.js"></script>
    <script src="../js/main.js"></script>
</div>
</body>
</html>