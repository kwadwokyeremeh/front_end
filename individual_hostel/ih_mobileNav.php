<div class="vk-top-header" style="background-color: #9c6f04">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="vk-top-header-left">

                    <span>Call Manager (+233) 123 456789</span>
                </div>
            </div>
            <div class="col-md-4 vk-top-header-right">
                <div class="vk-top-header-right">
                    <ul>
                        <li><a href="../index.php"> ehostelry </a></li>
                        <li> </li>
                        <li><a href="tel:0205806467"> Call Manager </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<nav class="visible-sm visible-xs mobile-menu-container mobile-nav vk-menu-mobile-nav">
    <div class="menu-mobile-nav navbar-toggle">
        <span class="icon-bar"><i class="fa fa-bars" aria-hidden="true"></i></span>
        <span class="icon-search"><i class="fa fa-search" aria-hidden="true"></i></span>
    </div>
    <div id="cssmenu" class="animated">
        <div class="uni-icon-close">
            <span class="ti-close"></span>
        </div>
        <ul class="nav navbar-nav">

            <li><a href="#">Pages</a>
                <ul class="sub-menu1 animated fadeIn">
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../general_hostels.php">Hostels</a></li>
                    <li><a href="../general_hostels.php">Homestels</a></li>
                    <li><a href="">Halls</a></li>
                    <li><a href="">Help</a></li>
                </ul>
            </li>
            <li class="sub"><a href="#overview">Overview</a>

            </li>

            <li class="sub"><a href="#room">Room Type</a>
            </li>

            <li class="sub"><a href="#amenities">Amenities</a>
            </li>
            <li class="sub"><a href="#location">Location</a>

            </li>
            <li class="sub"><a href="individual_hostel/individual_hostel_R&C.php">Reviews and Comments</a>

            </li>
            <li class="sub"><a href="#">Register</a>

            </li>
            <li class="sub"><a href="../signin.php">Sign in</a>

            </li>

        </ul>


        <div class="uni-nav-mobile-bottom">
            <div class="form-search-wrapper-mobile">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="I am here to help">
                    <span class="input-group-addon success"><i class="fa fa-search"></i></span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</nav>