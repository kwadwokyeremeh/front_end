<!doctype html>
<html lang="en">
<?php include('_partials/booking_head.php');?>
<body>

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<div class="vk-sparta-transparents-1">
    <!-- Mobile nav -->
    <?php include('_partials/booking_mobileNav.php');?>
     <!-- End mobile menu -->

    <div id="wrapper-container" class="site-wrapper-container">
        <!-- Main navigation -->
        <?php include('_partials/booking_mainNav.php');?>
        <!-- end of Main navigation -->


        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Reserve a bed</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-select-room-content">
                <div class="container">
                    <div class="vk-select-room-breakcrumb">
                        <ul>
                            <li class="completed">
                                <a href="javascript:void(0);">1. Personal Information</a>
                            </li>
                            <li class="active">
                                <a href="javascript:void(0);">2. Hostel</a>
                                <span class="round-tabs five">
                             <i class="fa fa-check" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">3. Choose a room</a>
                                <span class="round-tabs five">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">4. Payment</a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">5. Confirmation</a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                        </ul>
                    </div>
                    <div class="vk-shop-checkout-body">
                        <div class="container">
                            <main id="main" class="clearfix right_sidebar">
                                <div class="tg-container">
                                    <div id="primary">


                                        <div class="entry-thumbnail">

                                            <div class="entry-content-text-wrapper clearfix">
                                                <div class="entry-content-wrapper">
                                                    <div class="entry-content">
                                                        <div class="woocommerce">
                                                            <div class="woocommerce-info">
                                                                <i class="fa fa-info-circle" aria-hidden="true"></i> Returning customer
                                                                <a href="#" class="showcoupon click-here-to-login">Click here to login </a>
                                                            </div>
                                                            <div class="vk-form-woo-login">
                                                                <div class="row">
                                                                    <form class="woocomerce-form woocommerce-form-login login" method="post" >
                                                                        <div class="col-md-12">
                                                                            <p>If you shopped with us before, please enter your details in the boxes below.
                                                                                If you are a new customer, please proceed to the Registration section.
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="form-row form-row-first">
                                                                                <input type="text" class="input-text" name="username" id="username" placeholder="Username or email">
                                                                            </p>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="form-row form-row-last">
                                                                                <input class="input-text" type="password" name="password" id="password" placeholder="Password">
                                                                            </p>
                                                                        </div>
                                                                        <div class="clear"></div>


                                                                        <div class="col-md-12">
                                                                            <div class="vk-checkout-login">
                                                                                <div class="row">
                                                                                    <div class="col-md-9">
                                                                                        <p class="form-row">
                                                                                            <input type="submit" class="button" name="login" value="LOGIN">
                                                                                            <input type="hidden" name="redirect" value="">
                                                                                            <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                                                                                <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever">
                                                                                                <span>Remember me</span>
                                                                                            </label>
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <p class="lost_password">
                                                                                            <a href="../email_password.php"><i class="fa fa-question-circle" aria-hidden="true"></i>Lost your password?</a>
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </form>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="vk-checkout-billing-left">
                                                                    <div class="col-md-12">
                                                                        <div class="woocommerce-billing-fields">

                                                                            <h3>Personal details</h3>
                                                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                                                <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                    <label for="first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="first_name" id="first_name" placeholder="" value="" autocomplete="given-name">
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                    <label for="last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="last_name" id="last_name" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="phone" data-priority="20">
                                                                                    <label for="phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="tel" class="input-text " name="phone" id=phone" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="emaill" data-priority="20">
                                                                                    <label for="email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="email" class="input-text " name="email" id="email" placeholder="" value="" >
                                                                                </p>
                                                                                <div class="form-row form-row-last validate-required" id="sex_field_detail">

                                                                                <div role="radiogroup" class="">
                                                                                    <label for="sex" class="">Sex</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <label class="radio-inline" for="inline-radio1">
                                                                                        <input type="radio" id="inline-radio1" name="inline-radios" value="female" class="custom-control-input">
                                                                                        <span aria-hidden="true" class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description">Female</span>
                                                                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <label class="radio-inline" for="inline-radio2">
                                                                                        <input type="radio" id="inline-radio2" name="inline-radios" value="male" class="custom-control-input">
                                                                                        <span aria-hidden="true" class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description">Male</span>
                                                                                    </label>
                                                                                </div>
                                                                                </div>


                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="vk-checkout-order-paypal">
                                                                <div class="row">
                                                                    <div id="order_review" class="woocommerce-checkout-review-order">


                                                                        <div class="col-md-12">
                                                                            <div id="payment" class="woocommerce-checkout-payment">

                                                                                <div class="form-row place-order">
                                                                                    <noscript>
                                                                                        Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                                                                    </noscript>


                                                                                    <a href="booking_hostel.php"><input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="next" value="next" data-value="Next"></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- .entry-content -->
                                                </div>
                                            </div>
                                        </div>


                                    </div> <!-- Primary end -->
                                </div>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!--END CONTENT ABOUT-->


        <?php include('_partials/booking_footer.php');?>

    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery1.min.js"></script>
    <script src="../plugin/dist/owl.carousel.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.waypoints.js"></script>
    <script src="../js/number-count/jquery.counterup.min.js"></script>
    <script src="../js/isotope.pkgd.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/bootstrap-datepicker.tr.min.js"></script>
    <script src="../js/moment.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script src="../js/picturefill.min.js"></script>
    <script src="../js/lightgallery.js"></script>
    <script src="../js/lg-pager.js"></script>
    <script src="../js/lg-autoplay.js"></script>
    <script src="../js/lg-fullscreen.js"></script>
    <script src="../js/lg-zoom.js"></script>
    <script src="../js/lg-hash.js"></script>
    <script src="../js/lg-share.js"></script>
    <script src="../js/jquery.nice-select.js"></script>
    <script src="../js/semantic.js"></script>
    <script src="../js/parallax.min.js"></script>
    <script src="../js/jquery.nicescroll.min.js"></script>
    <script src="../js/jquery.sticky.js"></script>
    <script src="../js/main.js"></script>
</div>
</body>
</html>