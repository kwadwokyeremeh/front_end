<!DOCTYPE html>
<html lang="en">
<head>
    <title>E-hostelry | Sign In</title>

    <?php include('_partials/head.php'); ?>

</head>

<body>


<?php include('_partials/mobileNav.php'); ?>

<div id="wrapper-container" class="site-wrapper-container">


<?php include('_partials/mainNav.php'); ?>


<div id="main-content" class="site-main-content">
    <div id="home-main-content" class="site-home-main-content">

        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Register</h2>
                            <h3>
                                <a href="#">Start your session by signing up</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-shop-checkout-body">
                <div class="container">
                    <main id="main" class="clearfix right_sidebar">
                        <div class="tg-container">
                            <div id="primary">


                                <div class="entry-thumbnail">

                                    <div class="entry-content-text-wrapper clearfix">
                                        <div class="entry-content-wrapper">
                                            <div class="entry-content">
                                                <div class="woocommerce">
                                                    <div class="woocommerce-info" style="margin-bottom: 1px">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i> Registration
                                                        <div class="pull-right">
                                                            <p>Already registered, <a href="signin.php">Click here to sign in</a></p>
                                                        </div>
                                                    </div>
                                                    <div class="vk-checkout-login">

                                                        <div class="row">
                                                            <div class="vk-checkout-billing-left">
                                                                <div class="col-md-12">
                                                                    <div class="woocommerce-billing-fields">

                                                                        <h3>Enter your personal details</h3>
                                                                        <div class="woocommerce-billing-fields__field-wrapper">
                                                                            <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                <label for="first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="text" class="input-text " name="first_name" id="first_name" placeholder="" value="" autocomplete="given-name">
                                                                            </p>
                                                                            <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                <label for="last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="text" class="input-text " name="last_name" id="last_name" placeholder="" value="" >
                                                                            </p>
                                                                            <p class="form-row form-row-last validate-required" id="phone" data-priority="20">
                                                                                <label for="phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="tel" class="input-text " name="phone" id=phone" placeholder="" value="" >
                                                                            </p>
                                                                            <p class="form-row form-row-last validate-required" id="email" data-priority="20">
                                                                                <label for="email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="email" class="input-text " name="email" id="email" placeholder="" value="" >
                                                                            </p>
                                                                            <p class="form-row form-row-last validate-required" id="password" data-priority="20">
                                                                                <label for="password" class="">Password <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="password" class="input-text " name="password" id="password" placeholder="" value="" >
                                                                            </p>
                                                                           <p class="form-row form-row-last validate-required" id="password" data-priority="20">
                                                                                <label for="password" class="">Confirm Password <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="password" class="input-text " name="confirm_password" id="password" placeholder="" value="" >
                                                                            </p>

                                                                            <div class="form-row form-row-last validate-required" id="sex_field_detail">
                                                                                <div role="radiogroup" class="">
                                                                                    <label for="sex" class="">Sex</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <label class="radio-inline" for="inline-radio1">
                                                                                        <input type="radio" id="inline-radio1" name="inline-radios" value="female" class="custom-control-input">
                                                                                        <span aria-hidden="true" class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description">Female</span>
                                                                                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <label class="radio-inline" for="inline-radio2">
                                                                                        <input type="radio" id="inline-radio2" name="inline-radios" value="male" class="custom-control-input">
                                                                                        <span aria-hidden="true" class="custom-control-indicator"></span>
                                                                                        <span class="custom-control-description">Male</span>
                                                                                    </label>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                            <div class="vk-checkout-order-paypal">
                                                                <div class="row">
                                                                    <div id="order_review" class="woocommerce-checkout-review-order">


                                                                        <div class="col-md-12">
                                                                            <div id="payment" class="woocommerce-checkout-payment">

                                                                                <div class="form-row place-order">
                                                                                    <noscript>
                                                                                        Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                                                                    </noscript>


                                                                                    <a href="dashboard/student/student.php"><input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="register" value="Register" data-value="register"></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>
                                            </div><!-- .entry-content -->
                                        </div>
                                    </div>
                                </div>


                            </div> <!-- Primary end -->
                        </div>
                    </main>
                </div>
            </div>
        </section>


    </div>
</div>



</div>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery1.min.js"></script>
<script src="../plugin/dist/owl.carousel.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/number-count/jquery.counterup.min.js"></script>
<script src="../js/isotope.pkgd.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/bootstrap-datepicker.min.js"></script>
<script src="../js/bootstrap-datepicker.tr.min.js"></script>
<script src="../js/moment.min.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="../js/picturefill.min.js"></script>
<script src="../js/lightgallery.js"></script>
<script src="../js/lg-pager.js"></script>
<script src="../js/lg-autoplay.js"></script>
<script src="../js/lg-fullscreen.js"></script>
<script src="../js/lg-zoom.js"></script>
<script src="../js/lg-hash.js"></script>
<script src="../js/lg-share.js"></script>
<script src="../js/jquery.nice-select.js"></script>
<script src="../js/semantic.js"></script>
<script src="../js/parallax.min.js"></script>
<script src="../js/jquery.nicescroll.min.js"></script>
<script src="../js/jquery.sticky.js"></script>
<script src="../js/main.js"></script>
</body>
</html>
