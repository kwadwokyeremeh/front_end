<!doctype html>
<html lang="en">
<?php include('_partials/head.php');?>
<body>

<!--load page-->
<div class="load-page">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

<div class="vk-sparta-transparents-1">
    <!-- Mobile nav -->
    <?php include('_partials/mobileNav.php');?>
    <!-- End mobile menu -->

    <div id="wrapper-container" class="site-wrapper-container">
        <!-- Main navigation -->
        <?php include('_partials/mainNav.php');?>
        <!-- end of Main navigation -->


        <!--BENGIN CONTENT HEADER-->
        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Register your hostel</h2>
                            <h3>
                                <a href="#">Register your hostel</a>
                                <span><i class="fa fa-angle-right" aria-hidden="true"></i></span>
                                <a href="#">Basic Information</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="vk-select-room-content">
                <div class="container">
                    <div class="vk-select-room-breakcrumb">
                        <ul>
                            <li class="active">
                                <a href="javascript:void(0);">1.Basic Info</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><small>2.Hostel Details</small></a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">3.Add media</a>
                                <span class="round-tabs five">
                              <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">4.Amenities</a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);"><small>5.Layout and Pricing</small></a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">6.Policies</a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                            <li>
                                <a href="javascript:void(0);">7.Payment</a>
                                <span class="round-tabs five">
                             <i class="fa fa-angle-right" aria-hidden="true"></i>
                         </span>
                            </li>
                        </ul>
                    </div>
                    <div class="vk-shop-checkout-body">
                        <div class="container">
                            <main id="main" class="clearfix right_sidebar">
                                <div class="tg-container">
                                    <div id="primary">


                                        <div class="entry-thumbnail">

                                            <div class="entry-content-text-wrapper clearfix">
                                                <div class="entry-content-wrapper">
                                                    <div class="entry-content">
                                                        <div class="woocommerce">

                                                            <div class="row">
                                                                <div class="vk-checkout-billing-left">
                                                                    <div class="col-md-12">
                                                                        <div class="woocommerce-billing-fields">

                                                                            <h3>Hostel Manager Details</h3>
                                                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                                                <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                    <label for="title" class="">Title <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="" class=" " name="title" id="title" placeholder="" value="" autocomplete="given-name">
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                    <label for="first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="first_name" id="first_name" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                    <label for="last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="last_name" id="last_name" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="email" data-priority="20">
                                                                                    <label for="email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="email" class="input-text " name="email" id="email" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="phone" data-priority="20">
                                                                                    <label for="phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="tel" class="input-text " name="phone" id=phone" placeholder="" value="" >
                                                                                </p>


                                                                            </div>
                                                                       <h3>Hostel Portal Details</h3>
                                                                            <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                <label for="title" class="">Title <abbr class="required" title="required">*</abbr></label>
                                                                                <input type="" class=" " name="title" id="title" placeholder="" value="" autocomplete="given-name">
                                                                            </p>
                                                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                                                <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                    <label for="first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="first_name" id="first_name" placeholder="" value="" autocomplete="given-name">
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                    <label for="last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="last_name" id="last_name" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="phone" data-priority="20">
                                                                                    <label for="phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="tel" class="input-text " name="phone" id=phone" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="email" data-priority="20">
                                                                                    <label for="email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="email" class="input-text " name="email" id="email" placeholder="" value="" >
                                                                                </p>


                                                                            </div>
                                                                       <h3>Hostel Owner Details</h3>
                                                                            <div class="woocommerce-billing-fields__field-wrapper">
                                                                                <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                    <label for="title" class="">Title <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="" class=" " name="title" id="title" placeholder="" value="" autocomplete="given-name">
                                                                                </p>
                                                                                <p class="form-row form-row-first validate-required woocommerce-invalid woocommerce-invalid-required-field" id="billing_first_name_field_detail" data-priority="10">
                                                                                    <label for="first_name" class="">First name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="first_name" id="first_name" placeholder="" value="" autocomplete="given-name">
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="billing_last_name_field_detail" data-priority="20">
                                                                                    <label for="last_name" class="">Last name <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="text" class="input-text " name="last_name" id="last_name" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="phone" data-priority="20">
                                                                                    <label for="phone" class="">Phone <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="tel" class="input-text " name="phone" id=phone" placeholder="" value="" >
                                                                                </p>
                                                                                <p class="form-row form-row-last validate-required" id="email" data-priority="20">
                                                                                    <label for="email" class="">Email <abbr class="required" title="required">*</abbr></label>
                                                                                    <input type="email" class="input-text " name="email" id="email" placeholder="" value="" >
                                                                                </p>


                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>



                                                                <div class="vk-checkout-order-paypal">
                                                                    <div class="row">
                                                                        <div id="order_review" class="woocommerce-checkout-review-order">


                                                                            <div class="col-md-12">
                                                                                <div id="payment" class="woocommerce-checkout-payment">

                                                                                    <div class="form-row place-order">
                                                                                        <noscript>
                                                                                            Since your browser does not support JavaScript, or it is disabled, please ensure you click the &lt;em&gt;Update Totals&lt;/em&gt; button before placing your order. You may be charged more than the amount stated above if you fail to do so.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Update totals" /&gt;
                                                                                        </noscript>


                                                                                        <a href="02_hostel_details.php"><input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="next" value="next" data-value="Next"></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div><!-- .entry-content -->
                                                    </div>
                                                </div>
                                            </div>


                                        </div> <!-- Primary end -->
                                    </div>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!--END CONTENT ABOUT-->


        <?php include('_partials/footer.php');?>

    </div>
    <!-- Latest compiled and minified JavaScript -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery1.min.js"></script>
    <script src="../plugin/dist/owl.carousel.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.waypoints.js"></script>
    <script src="../js/number-count/jquery.counterup.min.js"></script>
    <script src="../js/isotope.pkgd.min.js"></script>
    <script src="../js/jquery-ui.min.js"></script>
    <script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/bootstrap-datepicker.tr.min.js"></script>
    <script src="../js/moment.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
    <script src="../js/picturefill.min.js"></script>
    <script src="../js/lightgallery.js"></script>
    <script src="../js/lg-pager.js"></script>
    <script src="../js/lg-autoplay.js"></script>
    <script src="../js/lg-fullscreen.js"></script>
    <script src="../js/lg-zoom.js"></script>
    <script src="../js/lg-hash.js"></script>
    <script src="../js/lg-share.js"></script>
    <script src="../js/jquery.nice-select.js"></script>
    <script src="../js/semantic.js"></script>
    <script src="../js/parallax.min.js"></script>
    <script src="../js/jquery.nicescroll.min.js"></script>
    <script src="../js/jquery.sticky.js"></script>
    <script src="../js/main.js"></script>
</div>
</body>
</html>