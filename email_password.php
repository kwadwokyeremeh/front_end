<!DOCTYPE html>
<html lang="en">
<head>
    <title>Roomie | Reset Password</title>

    <?php include('_partials/head.php'); ?>

</head>

<body>


<?php include('_partials/mobileNav.php'); ?>

<div id="wrapper-container" class="site-wrapper-container">


<?php include('_partials/mainNav.php'); ?>


    <div id="main-content" class="site-main-content">
        <div id="home-main-content" class="site-home-main-content">


        <section class="site-content-area">
            <div class="vk-gallery-grid-full-banner">
                <div class="vk-about-banner">
                    <div class="vk-about-banner-destop">
                        <div class="vk-banner-img"></div>
                        <div class="vk-about-banner-caption">
                            <h2>Reset Password</h2>
                            <h3>
                                <a href="#">Enter your email to receive a reset link</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vk-shop-checkout-body">
                <div class="container">
                    <main id="main" class="clearfix right_sidebar">
                        <div class="tg-container">
                            <div id="primary">


                                <div class="entry-thumbnail">

                                    <div class="entry-content-text-wrapper clearfix">
                                        <div class="entry-content-wrapper">
                                            <div class="entry-content">
                                                <div class="woocommerce">
                                                    <div class="woocommerce-info">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i> Password Reset Form

                                                    </div>
                                                    <div class="vk-checkout-login">
                                                        <div class="row">
                                                            <form class="woocomerce-form woocommerce-form-login login" method="post" >
                                                                <div class="col-md-12">
                                                                    <p>Please enter your email in the box below.
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <p class="form-row form-row-first">
                                                                        <label for="email" class="">Email Address <abbr class="required" title="required">*</abbr></label>
                                                                        <input type="text" class="input-text" name="email" id="email" placeholder="Please enter your email">
                                                                    </p>
                                                                </div>

                                                                <div class="clear"></div>


                                                                <div class="col-md-12">
                                                                    <div class="vk-checkout-login">
                                                                        <div class="row">
                                                                            <div class="col-md-9">
                                                                                <p class="form-row">
                                                                                    <input type="hidden" id="wpnonce" name="_wpnonce" value="b6598ef61e">
                                                                                    <input type="hidden" name="_wp_http_referer" value="/structure-contruction/checkout/">
                                                                                    <input type="submit" class="button" name="login" value="Send Password Reset Link">
                                                                                    <input type="hidden" name="redirect" value="">
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <p class="lost_password">
                                                                                    <a href="signin.php"><i class="fa fa-question-circle" aria-hidden="true"></i>Remembered, sign in</a>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="clear"></div>
                                                            </form>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>
                                            </div><!-- .entry-content -->
                                        </div>
                                    </div>
                                </div>


                            </div> <!-- Primary end -->
                        </div>
                    </main>
                </div>
            </div>
        </section>
    </div>
</div>



</div>
<!-- Latest compiled and minified JavaScript -->
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery1.min.js"></script>
<script src="../plugin/dist/owl.carousel.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/number-count/jquery.counterup.min.js"></script>
<script src="../js/isotope.pkgd.min.js"></script>
<script src="../js/jquery-ui.min.js"></script>
<script src="../js/bootstrap-datepicker.min.js"></script>
<script src="../js/bootstrap-datepicker.tr.min.js"></script>
<script src="../js/moment.min.js"></script>
<script src="../js/wow.min.js"></script>
<script src="../js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="../js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>
<script src="../js/picturefill.min.js"></script>
<script src="../js/lightgallery.js"></script>
<script src="../js/lg-pager.js"></script>
<script src="../js/lg-autoplay.js"></script>
<script src="../js/lg-fullscreen.js"></script>
<script src="../js/lg-zoom.js"></script>
<script src="../js/lg-hash.js"></script>
<script src="../js/lg-share.js"></script>
<script src="../js/jquery.nice-select.js"></script>
<script src="../js/semantic.js"></script>
<script src="../js/parallax.min.js"></script>
<script src="../js/jquery.nicescroll.min.js"></script>
<script src="../js/jquery.sticky.js"></script>
<script src="../js/main.js"></script>
</body>
</html>